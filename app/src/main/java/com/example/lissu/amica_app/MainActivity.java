package com.example.lissu.amica_app;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity implements VolleyCallback {
    private static final String TAG = "MAIN";

    MenuEngine menuEngine;

    Snackbar snackbarVolleyError;
    Snackbar snackbarVolleyFetching;

    private RecyclerView recyclerMenu;
    private RecyclerView.Adapter menuAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private TextView txtRestaurant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerMenu = findViewById(R.id.recyclerMenu);
        txtRestaurant = findViewById(R.id.txtRestaurantName);

        // for performance reason
        recyclerMenu.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerMenu.setLayoutManager(layoutManager);

        menuEngine = new MenuEngine(getApplicationContext(), this);

        txtRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "txtRestaurantName clicked");
                showOptionsDialog();
            }
        });

        // DEBUG
        // Utility.removePrefs(getApplicationContext());
    }

    private void showOptionsDialog() {
        // dialog setup
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_restaurant);

        RadioGroup rg = dialog.findViewById(R.id.radio_group);

        // dynamically creating radio buttons
        for(int i = 0; i < Utility.restaurants.length; i++){
            RadioButton rb = new RadioButtonParcel(this, Utility.restaurants[i]);
            rb.setText(Utility.restaurants[i].getRestaurantName());
            rb.setTextSize(16);
            rb.setTextColor(ContextCompat.getColor(this, R.color.dialog_radio_text));
            rb.setButtonTintList(ContextCompat.getColorStateList(this, R.color.dialog_radio_bg));

            rb.setPadding(0,25,0,25);
            rg.addView(rb);
        }

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int childCount = group.getChildCount();
                for (int x = 0; x < childCount; x++) {
                    RadioButton btn = (RadioButtonParcel) group.getChildAt(x);
                    if (btn.getId() == checkedId) {
                        Log.d(TAG, "selected restaurant: " + btn.getText().toString() + " " + ((RadioButtonParcel) btn).getRestaurantID());

                        // fetch id inside the button and save it to shared prefs
                        menuEngine.setRestaurant(((RadioButtonParcel) btn).getRestaurantID(), ((RadioButtonParcel) btn).getRestaurantCompany());
                        menuEngine.refreshData();

                        // closes the dialog after 500 ms
                        final Timer closeTimer = new Timer();
                        closeTimer.schedule(new TimerTask() {
                            public void run() {
                                dialog.dismiss();
                                closeTimer.cancel();
                            }
                        }, 500);
                    }
                }
            }
        });

        dialog.show();
    }

    // VolleyCallBack interface implementation
    // this is called when json is loaded and ready for recycler view
    @Override
    public void volleyOnSuccess(List<Day> days) {
        menuAdapter = new MenuAdapter(days, getApplicationContext(), this);
        recyclerMenu.setAdapter(menuAdapter);

        volleyDismiss();
    }

    @Override
    public void volleyError() {
        Log.d("TAG", "CALLED: volleyError");

        volleyDismiss();

        snackbarVolleyError = Snackbar.make(findViewById(android.R.id.content), "Ei Internet-yhteyttä", Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(Color.RED);

        snackbarVolleyError.setAction("Yritä uudelleen", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "snackbar click");
                menuEngine.refreshData();
                snackbarVolleyError = null;
            }
        });

        snackbarVolleyError.show();
    }

    @Override
    public void volleyFetching() {
        Log.d("TAG", "CALLED: volleyFetching");

        volleyDismiss();

        if (snackbarVolleyFetching != null)
            snackbarVolleyFetching.dismiss();

        snackbarVolleyFetching = Snackbar.make(findViewById(android.R.id.content), "Haetaan tietoja", Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(Color.RED);

        snackbarVolleyFetching.show();
    }

    @Override
    public void volleyUpdateRestaurantName(String name) {
        Log.d("TAG", "CALLED: volleyUpdateRestaurantName");
        txtRestaurant.setText(name);
    }

    @Override
    public void volleyDismiss() {
        if (snackbarVolleyError != null) {
            snackbarVolleyError.dismiss();
        }

        if (snackbarVolleyFetching != null) {
            snackbarVolleyFetching.dismiss();
        }
    }

    @Override
    public void volleyNoRestaurant() {
        showOptionsDialog();
    }

    @Override
    protected void onResume() {
        super.onResume();

        menuEngine.refreshData();
    }
}

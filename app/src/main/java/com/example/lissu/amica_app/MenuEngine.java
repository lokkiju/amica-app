package com.example.lissu.amica_app;

import android.content.Context;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MenuEngine {
    static final String TAG = "MENU_ENGINE";

    private String selectedRestaurant;
    private String selectedCompany;

    Context context;
    VolleyCallback volleyCallback;

    MenuEngine(Context context, VolleyCallback volley) {
        this.context = context;
        this.volleyCallback = volley;

        this.selectedRestaurant = Utility.loadPref(context, "restaurant");
        this.selectedCompany = Utility.loadPref(context, "company");

        Log.e("GETRESTAURANT", "restaurant: " + getRestaurant());
        if (selectedRestaurant.equals("")) {
            volleyCallback.volleyNoRestaurant();
        } else {
            refreshData();
        }
    }

    public void refreshData() {
        fetchJson();
    }

    private void fetchJson() {
        JsonGet json = new JsonGet(getDate("yyyy-MM-dd"), getRestaurant(), getCompany(), "fi", context, volleyCallback);
    }

    private String getDate(String dateformat) {
        Log.d(TAG, "CALLED: getDate");

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(dateformat);
        Integer dow = c.get(Calendar.DAY_OF_WEEK) - 2;

        if (dow == 5) {
            Log.d(TAG, "saturday, skipping to monday");
            c.add(Calendar.DATE, 2);
        } else if (dow == 6) {
            Log.d(TAG, "sunday, skipping to monday");
            c.add(Calendar.DATE, 1);
        }

        String date = dateFormat.format(c.getTime());
        Log.d(TAG, "date: " + date);
        return date;
    }

    public void setRestaurant(String restaurant, String company) {
        this.selectedRestaurant = restaurant;
        this.selectedCompany = company;

        Utility.savePrefs(context, this);
    }

    public String getRestaurant() {
        return selectedRestaurant;
    }

    public String getCompany() {
        return selectedCompany;
    }
}

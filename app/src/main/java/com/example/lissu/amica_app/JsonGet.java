package com.example.lissu.amica_app;


import android.content.Context;
import android.util.Log;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

interface VolleyCallback {
    void volleyNoRestaurant();
    void volleyOnSuccess(List<Day> days);
    void volleyError();
    void volleyFetching();
    void volleyUpdateRestaurantName(String name);
    void volleyDismiss();
}

class JsonGet {
    private static final String TAG = "JSON";

    private String date;
    private Context context;
    private String language;
    private VolleyCallback callback;

    JsonGet(String date, String restaurant, String company, String language, Context context, VolleyCallback callback) {
        this.date = date;
        this.context = context;
        this.language = language;
        this.callback = callback;

        String url = "";
        if (company.equals("AMICA")) {
            url = "https://www.amica.fi/modules/json/json/Index" +
                    "?costNumber=" + restaurant +
                    "&language=" + language;
        } else if(company.equals("FAZER")) {
            url = "https://www.fazerfoodco.fi/modules/json/json/Index" +
                    "?costNumber=" + restaurant +
                    "&language=" + language;
        } else {
            Log.d(TAG, "unknown company");
        }

        Log.d(TAG, "constructor got this url: " + url);
        if (!url.equals("")) {
            getJsonData(url);
        } else {
            Log.d(TAG, "url equls nothing!");
        }
    }

    public void getJsonData(String url) {
        Log.d(TAG, "CALLED: getJsonData");

        callback.volleyFetching();

        RequestQueue queue = Volley.newRequestQueue(context);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG + "_VOLLEY", "json response: " + response.toString());
                        callback.volleyOnSuccess(parseJsonData(response));
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG + "_VOLLEY", "json error response");
                        error.printStackTrace();

                        // clear any previous snackbars
                        callback.volleyDismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            callback.volleyError();
                        }
                    }
                });

        queue.add(jsObjRequest);
    }

    private List<Day> parseJsonData(JSONObject response) {
        Log.d(TAG, "CALLED: parseJsonData");
        Log.d(TAG, "response: " + response.toString());
        List<Day> parsedDays = new ArrayList<>();

        String restaurantName = "";

        try {
            // restaurant name
            restaurantName = response.getString("RestaurantName");

            // if no proper json response, assume it's because of language selection
            if (restaurantName.equals("null")) {
                restaurantName = "No english menu available";
            } else {
                callback.volleyUpdateRestaurantName(restaurantName);
            }

            // contains the whole week
            JSONArray menusForDays = response.getJSONArray("MenusForDays");

            // TODO: maybe do some more error checking here

            // loop through days
            // contains: Date, LunchTime and SetMenus[]
            for (int d = 0; d < menusForDays.length(); d++) {
                // get date
                JSONObject menuForDate = menusForDays.getJSONObject(d);
                // TODO: do better than null
                String date = "null";
                String lunchtime = "null";

                if (menuForDate.getJSONArray("SetMenus").length() > 0) {
                    // example: "2018-10-16T00:00:00+02:00"
                    String menuDateTime = menuForDate.getString("Date");

                    // example: "Kotkanpoika 10.15-13.00/Kultturelli 10.30-16.00"
                    lunchtime = menuForDate.getString("LunchTime");

                    // date needs some parsing; split with t to remove useless characters
                    String[] menuDate = menuDateTime.split("T");
                    Log.d(TAG, "menuDate[0]: " + menuDate[0]);

                    if (menuDate.length > 0) {
                        // TODO: format time according to language
                        // TODO: and just generally make this less shittier
                        String[] dateNumbers = menuDate[0].split("-");
                        date = dateNumbers[2] + "." + dateNumbers[1] + "." + dateNumbers[0];

                        // get day
                        Calendar c = Calendar.getInstance();
                        c.setTime(new SimpleDateFormat("yyyy-mm-dd").parse(menuDate[0]));
                        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - 2;

                        if (dayOfWeek > 0 ||dayOfWeek < 7) {
                            String[] dayName = {
                                    "Maanantai",
                                    "Tiistai",
                                    "Keskiviikko",
                                    "Torstai",
                                    "Perjantai",
                                    "Lauantai",
                                    "Sunnuntai"};

                            String day = dayName[dayOfWeek];

                            date = day + " " + date;
                        } else {
                            Log.e(TAG, "day of week conversion error");
                        }
                    }
                }

                // if everything is null its safe to continue
                if (date.equals("null") && lunchtime.equals("null")) {
                    continue;
                }

                // create a day to be filled with menus
                Day day = new Day(date, lunchtime);

                // add actual menus; setMenu contains meals for one day and components are its meals
                // contains: SortOrder, Name, Price, Components[]
                // actual useful fields are Name and Components[], the rest are always zero or null
                JSONArray setMenus = menuForDate.getJSONArray("SetMenus");

                // loop through menus
                for (int i = 0; i < setMenus.length(); i++) {
                    JSONObject setMenu = setMenus.getJSONObject(i);
                    // example: "KASVISLOUNAS"
                    String setMenuName = setMenu.getString("Name");

                    // name might be null and but it could still have components (meals)
                    if (setMenuName == "null") {
                        if (setMenu.getJSONArray("Components").length() > 0) {
                            // null replace
                            // TODO: localization!
                            setMenuName = "Lisätietoja";
                        } else {
                            // if setMenuName is null and it has no components its safe to move on
                            continue;
                        }
                    }

                    // create a new menu to be further added with data
                    Menu menu = new Menu(setMenuName);

                    // Components array contains all the meals and their diets IN A SINGLE STRING.
                    // wonderful eh? so, we are left with some more parsing to do.
                    // example: "Lihakeittoa (* ,A ,G ,L ,M)"
                    JSONArray components = setMenu.getJSONArray("Components");

                    for (int c = 0; c < components.length(); c++) {
                        String component = components.get(c).toString();
                        Log.d(TAG, "component: " + component);

                        // componentNameDiet[0] = Lihakeittoa
                        // componentNameDiet[1] = * ,A ,G ,L ,M)
                        String[] componentNameDiet = component.split("\\(");
                        String nameString = "component_name_null";
                        String dietsString = "diets_null";
                        List<String> diets = new ArrayList<>();

                        if (componentNameDiet.length > 0) {
                            // nulled if null
                            // TODO: do better than null
                            nameString = componentNameDiet[0];

                            if (componentNameDiet.length > 1) {
                                dietsString = componentNameDiet[1];
                                // dietsSplit[0] = *
                                // dietsSplit[1] = A
                                // dietsSplit[2] = G
                                // dietsSplit[3] = L
                                // dietsSplit[4] = M)
                                String[] dietsSplit = dietsString.split(",");

                                for (String diet : dietsSplit) {
                                    // last diet needs some trimming
                                    // yes, very lazy. i am aware.
                                    // TODO: make it not lazy
                                    diets.add(diet.replace(")", "").trim());
                                }

                            } else {
                                // if diets do not exist, inform with null
                                // TODO: do something else than null
                                diets.add(dietsString);
                            }
                        } else {
                            // TODO: also here could do something more than just smack the user with endless nulls
                            diets.add("null");
                        }


                        menu.addMeal(new Meal(nameString, diets, context));
                    }

                    // and lastly save all the parsed data
                    day.addMenu(menu);
                }

                parsedDays.add(day);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // debug print parsed data
        for (Day day: parsedDays) {
            Log.d(TAG + "_PARSED", day.toString());

            for (Menu menu : day.getMenus()) {
                Log.d(TAG + "_PARSED", "\t" + menu.toString());

                for (Meal meal : menu.getMeals()) {
                    Log.d(TAG + "_PARSED", "\t\t" + meal.toString());
                }
            }
        }

        return ApiBubbleGum(parsedDays);
    }

    // TODO: this needs to be localized
    private List<Day> ApiBubbleGum(List<Day> parsedDays) {
        for(Day day : parsedDays) {
            // add some spaces around /
            String lunchtime = day.getLunchtime();
            String trimmedlunch = lunchtime.replace("/", " / ");
            day.setLunchtime(trimmedlunch);

            int lunch = 0;

            for (Menu menu : day.getMenus()) {
                // lunch counting
                if (menu.getName().equals("LOUNAS")) {
                    lunch++;
                }

                // remove salad if its empty
                if (menu.getName().equals("PÄIVÄNSALAATTI")) {
                    if (menu.getMeals().size() < 1) {
                        menu.setName("%salad%");
                    }
                }
            }

            if (lunch > 1) {
                // too many lunches, applying bubblegum
                lunch = 0;
                for (Menu menu : day.getMenus()) {
                    if (menu.getName().equals("LOUNAS")) {
                        lunch++;

                        if (lunch > 1) {
                            menu.setName("%lunch%");
                        }
                    }
                }
            }
        }

        // move lunches over soups and other things
        List<Day> sortedDays = new ArrayList<>();

        for(Day day : parsedDays) {
            List<Menu> sortedMenus = new ArrayList<>();

            for(Menu menu : day.getMenus()) {
                if (menu.getName().equals("LOUNAS")) {
                    sortedMenus.add(0, menu);
                    continue;
                }

                if (menu.getName().equals("%lunch%")) {
                    sortedMenus.add(1, menu);
                    continue;
                }

                sortedMenus.add(menu);
            }

            day.setMenus(sortedMenus);
            sortedDays.add(day);
        }

        return sortedDays;
    }
}

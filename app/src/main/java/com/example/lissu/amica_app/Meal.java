package com.example.lissu.amica_app;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Meal {
    static final String TAG = "MEAL";

    private String name;
    //private List<String> diets;
    private ArrayList<Map.Entry<String, Integer>> dietsColors = new ArrayList<Map.Entry<String, Integer>>();

    public Meal(String name, List<String> diets, Context context) {
        this.name = name;
        //this.diets = diets;

        for (String d : diets) {
            dietsColors.add(new AbstractMap.SimpleEntry(d, getDietColor(d, context)));
        }
    }

    public String getName() {
        return this.name;
    }

    /*public List<String> getDiets() {
        return this.diets;
    }*/

    public ArrayList<Map.Entry<String, Integer>> getDiets() {
        return dietsColors;
    }

    private int getDietColor(String d, Context context) {
        Log.d(TAG, "CALLED: getDietColor");
        Integer color;

        d = d.toLowerCase();

        switch (d) {
            case "g":
                color = ContextCompat.getColor(context, R.color.diet_g);
                break;

            case "l":
                color = ContextCompat.getColor(context, R.color.diet_l);
                break;

            case "vl":
                color = ContextCompat.getColor(context, R.color.diet_vl);
                break;

            case "m":
                color = ContextCompat.getColor(context, R.color.diet_m);
                break;

            case "*":
                color = ContextCompat.getColor(context, R.color.diet_star);
                break;

            case "veg":
                color = ContextCompat.getColor(context, R.color.diet_veg);
                break;

            case "vs":
                color = ContextCompat.getColor(context, R.color.diet_vs);
                break;

            case "a":
                color = ContextCompat.getColor(context, R.color.diet_a);
                break;

            default:
                color = 0;
                Log.d(TAG, "dietColor defaults");
        }

        return color;
    }

    /*
    @Override
    public String toString() {
        String d = "meal.name: " + this.name + "\tmeal.diets: ";

        for (String diet : diets) {
            d = d + diet + " ";
        }

        return d;
    }
    */
}

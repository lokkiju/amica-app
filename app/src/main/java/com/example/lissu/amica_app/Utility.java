package com.example.lissu.amica_app;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import static android.content.Context.MODE_PRIVATE;

public class Utility {
    private static final String TAG = "UTILITY";

    public static final Restaurant[] restaurants = {
            new Restaurant("Kotkanpoika & Kultturelli", "0235", "AMICA"),
            new Restaurant("Alwari", "0226", "AMICA"),
            new Restaurant("Wallu", "0217", "AMICA"),
            new Restaurant("Majakka", "2532", "AMICA"),
            new Restaurant("Fasaani", "0218", "AMICA"),
            new Restaurant("Aallokko", "3503", "AMICA"),
            new Restaurant("ODL Kantakortteli", "1694", "FAZER"),
    };

    private Utility() {}

    static <Any> Any loadPref(Context context, String pref) {
        Log.d(TAG, "CALLED: loadPref " + pref);
        SharedPreferences sharedPrefs;

        sharedPrefs = context.getSharedPreferences("prefs", MODE_PRIVATE);

        switch (pref) {
            case "restaurant":
                String restaurant = sharedPrefs.getString(pref, "");
                Log.d(TAG, "saved restaurant: " + restaurant);

                return (Any) restaurant;
            case "company":
                String company = sharedPrefs.getString(pref, "");
                Log.d(TAG, "saved company: " + company);

                return (Any) company;
            default:
                Log.d(TAG, "loadPref defaults");
        }

        return null;
    }

    static void savePrefs(Context context, MenuEngine menuEngine) {
        Log.d(TAG, "CALLED: savePrefs");

        SharedPreferences sharedPrefs;
        sharedPrefs = context.getSharedPreferences("prefs", MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPrefs.edit();

        // restaurant selection
        editor.putString("restaurant", menuEngine.getRestaurant());
        editor.putString("company" ,menuEngine.getCompany());
        editor.commit();
    }

    static void removePrefs(Context context) {
        Log.d(TAG, "CALLED: removePrefs");

        SharedPreferences sharedPrefs;
        sharedPrefs = context.getSharedPreferences("prefs", MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPrefs.edit();

        editor.remove("restaurant");

        editor.commit();
    }

    static Point getScreenResolution(Context context) {
        // get screen resolution
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm != null ? wm.getDefaultDisplay() : null;

        Point size = new Point();

        if (wm != null) {
            display.getSize(size);
        } else {
            Log.e(TAG, "cannot get default display, assuming fhd");
            size.x = 1080;
            size.y = 1920;
        }

        Log.d(TAG, "size.x: " + size.x + " size.y: " + size.y);
        return size;
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
}

class Restaurant {
    String name;
    String id;
    String company;

    Restaurant(String name, String id, String company) {
        this.name = name;
        this.id = id;
        this.company = company;
    }

    public String getRestaurantName() {
        return name;
    }

    public String getRestaurantID() {
        return id;
    }

    public String getRestaurantCompany() {
        return company;
    }
}

class RadioButtonParcel extends android.support.v7.widget.AppCompatRadioButton {
    Restaurant restaurant;

    public RadioButtonParcel(Context context, Restaurant restaurant) {
        super(context);
        this.restaurant = restaurant;
    }

    public String getRestaurantName() {
        return restaurant.getRestaurantName();
    }

    public String getRestaurantID() {
        return restaurant.getRestaurantID();
    }

    public String getRestaurantCompany() {
        return restaurant.getRestaurantCompany();
    }
}
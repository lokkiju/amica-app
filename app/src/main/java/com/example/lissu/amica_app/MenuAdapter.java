package com.example.lissu.amica_app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {
    private List<Day> days;
    private Context context;
    private MainActivity mainActivity;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout linear;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;

            linear = v.findViewById(R.id.linearRow);
        }
    }

    public void add(int position, Day item) {
        days.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        days.remove(position);
        notifyItemRemoved(position);
    }

    public MenuAdapter(List<Day> days, Context context, MainActivity mainActivity) {
        this.days = days;
        this.context = context;
        this.mainActivity = mainActivity;
    }

    @Override
    public MenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // get screen width to cut meals if they are too long
        Point size = Utility.getScreenResolution(context);
        int screenWidth = size.x;

        Day day = days.get(position);

        // day (Maanantai 1.1.1900 \n Kotkanpoika 10.15...)
        final String date = day.getDate();
        final String lunchtime = day.getLunchtime();

        TextView txtDate = new TextView(context);
        TextView txtLunchtime = new TextView(context);
        Typeface tfDate = Typeface.create("sans-serif-medium", Typeface.BOLD);
        Typeface tfLunch = Typeface.create("sans-serif-medium", Typeface.NORMAL);

        txtDate.setTypeface(tfDate);
        txtDate.setTextSize(15);
        txtDate.setTextColor(ContextCompat.getColor(context, R.color.textview_date));
        txtDate.setGravity(Gravity.CENTER_HORIZONTAL);
        txtDate.setBackgroundColor(ContextCompat.getColor(context, R.color.textview_date_bg));
        txtDate.setText(date);
        txtDate.setPadding(0,25,0,0);

        txtLunchtime.setTypeface(tfLunch);
        txtLunchtime.setTextSize(10);
        txtLunchtime.setTextColor(ContextCompat.getColor(context, R.color.textview_lunchtime));
        txtLunchtime.setGravity(Gravity.CENTER_HORIZONTAL);
        txtLunchtime.setBackgroundColor(ContextCompat.getColor(context, R.color.textview_lunchtime_bg));
        txtLunchtime.setText(lunchtime);
        txtLunchtime.setPadding(0,0,0,25);

        holder.linear.removeAllViews();
        holder.linear.addView(txtDate);
        holder.linear.addView(txtLunchtime);
        holder.linear.addView(addSpace(0,40));

        for (Menu menu : day.getMenus()) {
            LinearLayout linearVerticalMenu = new LinearLayout(context);
            holder.linear.addView(linearVerticalMenu);

            // menu (lounas, kasvislounas, ...)
            TextView txtMenuName = new TextView(context);
            Typeface tfMenuName = Typeface.create("sans-serif-medium", Typeface.NORMAL);

            txtMenuName.setTypeface(tfMenuName);
            txtMenuName.setTextSize(14);
            txtMenuName.setTextColor(ContextCompat.getColor(context, R.color.textview_menuset));

            String menuName = menu.getName();
            txtMenuName.setText(menuName);

            // these come from JsonGet.ApiBubbleGum()
            switch (menuName) {
                case "%lunch%":
                    linearVerticalMenu.addView(addSpace(0,1));
                    break;
                case "%salad%":
                    break;
                default:
                    txtMenuName.setPadding(20,0,0,0);
                    txtMenuName.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    txtMenuName.setBackgroundColor(ContextCompat.getColor(context, R.color.textview_menuset_bg));

                    linearVerticalMenu.addView(txtMenuName);
            }

            boolean isPointer = false;

            // meal (lihakeittoa, härkäpapukiusausta, ...)
            for (Meal meal : menu.getMeals()) {
                // horizontal linear layout for adding diet icons after meal name
                LinearLayout linearHorizontalMeal = new LinearLayout(context);
                linearHorizontalMeal.setGravity(Gravity.CENTER_VERTICAL);
                holder.linear.addView(linearHorizontalMeal);

                // meal will go into above created layout
                TextView txtMealName = new TextView(context);
                Typeface tfMealName = Typeface.create("sans-serif-light", Typeface.NORMAL);

                String mealName = meal.getName();

                txtMealName.setTypeface(tfMealName);
                txtMealName.setTextSize(14);
                txtMealName.setTextColor(ContextCompat.getColor(context, R.color.textview_meal));
                txtMealName.setText(mealName);
                txtMealName.setPadding(0,0,10,0);

                // calculate meal max length based on diets
                int maxStringWidth = screenWidth - (meal.getDiets().size() * Utility.dpToPx(23));
                Log.d("MEASURE", "maxStringWidth: " + maxStringWidth);

                txtMealName.measure(0,0);
                int mealNameLength = txtMealName.getMeasuredWidth();

                if (mealNameLength > maxStringWidth) {
                    // shorten textviews box to account for last word moving to second line
                    String[] mealNameSplit = mealName.split(" ");
                    float lengthPerChar = mealNameLength / mealName.length();
                    int mealNameLastLength = Math.round(lengthPerChar * mealNameSplit[mealNameSplit.length - 1].length());
                    maxStringWidth = mealNameLength - mealNameLastLength - 50;

                    Log.d("MEASURE", "mealNameLength: " + mealNameLength);
                    Log.d("MEASURE", "lengthPerChar: " + lengthPerChar);
                    Log.d("MEASURE", "last word: " + mealNameSplit[mealNameSplit.length - 1]);
                    Log.d("MEASURE", "last word chars: " + mealNameSplit[mealNameSplit.length - 1].length());
                    Log.d("MEASURE", "last word length: " + mealNameLastLength);
                    Log.d("MEASURE", "new maxStringWidth: " + maxStringWidth);
                }

                txtMealName.setMaxWidth(maxStringWidth);
                //txtMealName.setSingleLine();

                // draw pointer only to the first meal as two or more meals "belong together":
                // - Riistajauhelihakeittoa
                //   Kevyttä edamjuustoa
                TextView txtMealPointer = new TextView(context);
                txtMealPointer.setText("\u25AA");
                txtMealPointer.setPadding(25, 0, 10, 0);

                if (!isPointer) {
                    txtMealPointer.setTextColor(ContextCompat.getColor(context, R.color.textview_meal_pointer));
                    isPointer = true;
                } else {
                    txtMealPointer.setTextColor(ContextCompat.getColor(context, R.color.textview_meal_bg));
                }

                linearHorizontalMeal.addView(txtMealPointer);
                linearHorizontalMeal.addView(txtMealName);

                // diets are those colorful round things after meals
                // and they will also be included in the aforementioned layout
                ArrayList<Map.Entry<String, Integer>> diets = meal.getDiets();

                if (diets != null) {
                    for (int i = 0; i < diets.size(); i++) {
                        String diet = diets.get(i).getKey();
                        Integer color = diets.get(i).getValue();

                        TextView txtDiet = new TextView(context);
                        Typeface tfDiet = Typeface.create("sans-serif-medium", Typeface.NORMAL);

                        txtDiet.setTypeface(tfDiet);
                        txtDiet.setTextSize(5);
                        txtDiet.setGravity(Gravity.CENTER);
                        txtDiet.setTextColor(ContextCompat.getColor(context, R.color.diet_textcolor));
                        txtDiet.setBackground(ContextCompat.getDrawable(context, R.drawable.diet_round));
                        txtDiet.setMaxLines(1);

                        txtDiet.setText(diet.trim());
                        Drawable drawable = txtDiet.getBackground();
                        drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

                        linearHorizontalMeal.addView(txtDiet);
                        linearHorizontalMeal.addView(addSpace(8,85));
                    }
                }
            }

            holder.linear.addView(addSpace(0,20));
        }
    }

    private Space addSpace(Integer width, Integer height) {
        Space space = new Space(context);
        space.setMinimumWidth(width);
        space.setMinimumHeight(height);
        return space;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return days.size();
    }

}

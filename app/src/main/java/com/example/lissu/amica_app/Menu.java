package com.example.lissu.amica_app;

import java.util.ArrayList;
import java.util.List;

public class Menu {
private String name;
private List<Meal> meals;

public Menu(String name) {
        this.name = name;
        this.meals = new ArrayList<>();
        }

public void setName(String name) {
        this.name = name;
        }

public String getName() {
        return name;
        }

public List<Meal> getMeals() {
        return meals;
        }

public void addMeal(Meal meal) {
        meals.add(meal);
        }

@Override
public String toString() {
        return "menu.name: " + this.name + " meals.size: " + meals.size();
        }
        }

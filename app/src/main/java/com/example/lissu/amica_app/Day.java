package com.example.lissu.amica_app;

import java.util.ArrayList;
import java.util.List;

public class Day {
    private List<Menu> menus;
    private String date;
    private String lunchtime;

    Day(String date, String lunchtime) {
        this.date = date;
        this.lunchtime = lunchtime;

        menus = new ArrayList<>();
    }

    public void addMenu(Menu menu) {
        menus.add(menu);
    }

    public void setMenus(List <Menu> menus) {
        this.menus = menus;
    }

    public void setLunchtime(String lunchtime) {
        this.lunchtime = lunchtime;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public String getDate() {
        return this.date;
    }

    public String getLunchtime() {
        return this.lunchtime;
    }

    @Override
    public String toString() {
        return "day.date: " + this.date + " day.lunchtime: " + this.lunchtime + " menus.size: " + menus.size();
    }
}
